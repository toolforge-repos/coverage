// Copyright (C) 2017, 2023 Kunal Mehta <legoktm@debian.org>
// SPDX-License-Identifier: GPL-3.0-or-later
use anyhow::{anyhow, Result};
use chrono::{Duration, NaiveDate, NaiveDateTime};
use regex::Regex;
use reqwest::{header, Client};
use serde::Deserialize;
use std::fs;
use std::path::{Path, PathBuf};

const USER_AGENT: &str = toolforge::user_agent!("coverage");
const CORE_CLOVER: &str =
    "https://doc.wikimedia.org/cover/mediawiki-core/clover.xml";
const CLOVER_FORMAT: &str = "clover-%Y-%m-%d.xml";

async fn download_clover(
    client: &Client,
    url: &str,
    directory: PathBuf,
) -> Result<()> {
    println!("fetching {url}");
    let resp = client.get(url).send().await?;
    let modified = resp
        .headers()
        .get(header::LAST_MODIFIED)
        .ok_or_else(|| anyhow!("no last-modified header"))?
        .to_str()?
        .to_string();
    let modified =
        NaiveDateTime::parse_from_str(&modified, "%a, %d %b %Y %H:%M:%S GMT")?;
    if !directory.exists() {
        fs::create_dir_all(directory.clone())?;
    }
    let path =
        directory.join(modified.date().format(CLOVER_FORMAT).to_string());
    if path.exists() {
        println!("Already downloaded {}", path.display());
        return Ok(());
    }
    fs::write(path, resp.bytes().await?)?;
    Ok(())
}

#[derive(Debug, Deserialize)]
struct Coverage {
    project: Project,
}

#[derive(Debug, Deserialize)]
struct Project {
    metrics: Metrics,
}

#[derive(Debug, Deserialize)]
struct Metrics {
    #[serde(default)]
    conditionals: u64,
    elements: u64,
    #[serde(default)]
    methods: u64,
    #[serde(default)]
    statements: u64,

    #[serde(rename = "coveredconditionals")]
    #[serde(default)]
    covered_conditionals: u64,
    #[serde(rename = "coveredelements")]
    covered_elements: u64,
    #[serde(rename = "coveredmethods")]
    #[serde(default)]
    covered_methods: u64,
    #[serde(rename = "coveredstatements")]
    #[serde(default)]
    covered_statements: u64,
}

async fn fetch_list(name: &str, client: &Client) -> Result<Vec<String>> {
    let path = if name == "libs" {
        "cover".to_string()
    } else {
        format!("cover-{name}")
    };
    let req = client
        .get(format!("https://doc.wikimedia.org/{path}/"))
        .send()
        .await?;
    let mut list = vec![];
    let regex =
        Regex::new(r#"<a class="cover-item" href="./(.*?)/">"#).unwrap();
    for cap in regex.captures_iter(&req.text().await?) {
        list.push(cap[1].to_string());
    }
    Ok(list)
}

async fn fetch(
    client: &Client,
    extensions: &[String],
    libs: &[String],
) -> Result<()> {
    download_clover(client, CORE_CLOVER, PathBuf::from("./mediawiki/")).await?;
    for name in extensions {
        let path = PathBuf::from(format!("./extensions/{name}"));
        download_clover(
            client,
            &format!(
                "https://doc.wikimedia.org/cover-extensions/{name}/clover.xml"
            ),
            path,
        )
        .await?;
    }
    for name in libs {
        let path = PathBuf::from(format!("./libs/{name}"));
        download_clover(
            client,
            &format!("https://doc.wikimedia.org/cover/{name}/clover.xml"),
            path,
        )
        .await?;
    }
    Ok(())
}

fn parse_clover(path: &Path) -> Result<(NaiveDate, f32)> {
    let name = NaiveDate::parse_from_str(
        path.file_name().unwrap().to_str().unwrap(),
        CLOVER_FORMAT,
    )?;
    let clover: Coverage = serde_xml_rs::from_str(&fs::read_to_string(path)?)?;
    let metrics = clover.project.metrics;
    let total = metrics.conditionals
        + metrics.elements
        + metrics.methods
        + metrics.statements;
    let div = metrics.covered_conditionals
        + metrics.covered_elements
        + metrics.covered_methods
        + metrics.covered_statements;
    let covered = if total == 0 {
        0_f32
    } else {
        div as f32 / total as f32 * 100_f32
    };
    Ok((name, covered))
}

fn chart(directory: PathBuf, dest: PathBuf, name: String) -> Result<()> {
    use plotters::prelude::*;
    let mut data_points = vec![];
    let mut chart_domain = vec![];
    let mut final_total = 0.0;
    let mut paths = vec![];
    for entry in directory.read_dir()? {
        let path = entry?.path();
        if path.extension().unwrap().to_str().unwrap() == "xml" {
            paths.push(path);
        }
    }
    paths.sort();
    for path in paths {
        let (date, value) = parse_clover(&path)?;
        chart_domain.push(date);
        data_points.push((date, value));
        final_total = value;
    }
    let mut buf = String::new();
    let start_date = chart_domain[0];
    let end_date = chart_domain.last().unwrap();

    {
        let root_area =
            SVGBackend::with_string(&mut buf, (1600, 800)).into_drawing_area();
        root_area.fill(&WHITE)?;
        let mut ctx = ChartBuilder::on(&root_area)
            .caption(name, ("sans-serif", 50))
            .set_label_area_size(LabelAreaPosition::Left, 60)
            .set_label_area_size(LabelAreaPosition::Bottom, 60)
            // Expand the x-range by 60 days and y-range by 120% so we don't cut off the top of the chart
            .build_cartesian_2d(
                start_date..(*end_date + Duration::try_days(60).unwrap()),
                0.0..final_total * 1.20,
            )?;

        ctx.configure_mesh()
            .label_style(("sans-serif", 14))
            .x_desc("Date")
            .y_desc("Percentage covered")
            .draw()?;

        ctx.draw_series(LineSeries::new(data_points, &BLUE))?;
    }

    println!("Writing SVG to {}", dest.display());
    let parent_dir = dest.parent().unwrap();
    if !parent_dir.is_dir() {
        fs::create_dir_all(parent_dir)?;
    }
    fs::write(dest, buf)?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let client = Client::builder().user_agent(USER_AGENT).build()?;
    let extensions = fetch_list("extensions", &client).await?;
    let libs: Vec<_> = fetch_list("libs", &client)
        .await?
        .into_iter()
        .filter(|item| item != "mediawiki-core")
        .collect();
    fetch(&client, &extensions, &libs).await?;
    chart(
        PathBuf::from("./mediawiki/"),
        PathBuf::from("./charts/mediawiki.svg"),
        "MediaWiki core test coverage".to_string(),
    )?;
    for extension in extensions {
        chart(
            PathBuf::from(format!("./extensions/{extension}")),
            PathBuf::from(format!("./charts/{extension}.svg")),
            format!("{extension} test coverage"),
        )?;
    }
    for lib in libs {
        chart(
            PathBuf::from(format!("./libs/{lib}")),
            PathBuf::from(format!("./charts/libs/{lib}.svg")),
            format!("{lib} test coverage"),
        )?;
    }
    Ok(())
}
